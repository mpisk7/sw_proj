#include <systemc.h>

using namespace std;

SC_MODULE(hardware_module) {

	sc_in<int> program;

	void task0() {
		while (1) {
			if (program.read() == 2)
				cout << "Temperatura 2 na module sprzetowym!" << endl;
			wait(100, SC_NS);
		}
	}

	SC_CTOR(hardware_module) {
		SC_THREAD(task0);
	}
};
