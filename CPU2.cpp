#include <systemc.h>

using namespace std;

SC_MODULE(CPU2) {
	sc_fifo_in<int> program;

	int state;
	void task0() {
		while (1) {
			program.read(state);
			if (state == 3) {
				cout << "Temperatura 3 na CPU2!" << endl;
			}

		}
	}

	SC_CTOR(CPU2) {
		SC_THREAD(task0);
	}
};