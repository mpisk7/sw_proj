#include <systemc.h>
#include "CPU1.cpp"
#include "CPU2.cpp"
#include "hardware_module.cpp"

using namespace std;
 
SC_MODULE(que) {
	sc_fifo<int> Queue;

	SC_CTOR(que) {

		sc_fifo<int> Queue(6);
	}
};

int sc_main(int argc, char* argv[]) {

	sc_signal<int> programl;

	que queue("que");

	CPU1 procesor1("CPU1");
	procesor1.program(programl);
	procesor1.outForCpu2(queue.Queue);

	CPU2 procesor2("CPU2");
	procesor2.program(queue.Queue);

	hardware_module modul("hardware_module");
	modul.program(programl);

	sc_start();

	return(0);
}
