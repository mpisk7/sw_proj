#include <systemc.h>
#include <iostream>

using namespace std;

SC_MODULE(CPU1) {

	sc_inout<int> program;
	sc_fifo_out<int> outForCpu2;

	int p, state, previous;
	void task0() {
		cout << "Wybierz program: " << endl;
		cout << "[0] Zmien temperature " << endl;
		cout << "[1] Program 1: Temperatura 1" << endl;
		cout << "[2] Program 2: Temperatura 2 na module sprzetowym" << endl;
		cout << "[3] Program 3: Temperatura 3 na CPU 2 " << endl;
		cout << "[4] Program 4: Temperatura 4 " << endl;
		cout << "[5] Program 5: Temperatura 5 " << endl;
		cout << "[6] Program 6: Temperatura 6 " << endl;
		cout << "[7] Program 7: Temperatura 7 " << endl;
		cout << "[9] Wylacz kuchenke" << endl;

		previous = 0;
		while (p != 9) {
			cout << "Wybor: ";
			cin >> p;

			if (p >= 0 && p <= 7) {
				program.write(p);
			}
			else
				cout << "Wybrano zly program!" << endl;

			wait(10, SC_NS);


			state = program.read();
			if (state == 0) {
				cout << "Brak wybranego programu" << endl;
				previous = 0;
				program.write(0);
			}

			wait(100, SC_NS);

		}
		sc_stop();
	}

	void task1() {
		while (1) {
			state = program.read();
			if (state == 1) {
				if (previous == 0) {
					cout << "Temperatura 1" << endl;
					previous = 1;
				}
				else {
					cout << "ERROR!" << endl;
				}
			}
			wait(100, SC_NS);

		}
	}

	void task2() {
		while (1) {
			if (state == 2) {
				if (previous == 0) {
					previous = 2;
				}
				else {
					cout << "ERROR!" << endl;
				}
			}
			wait(100, SC_NS);

		}
	}

	void task3() {
		while (1) {
			state = program.read();
			if (state == 3) {
				if (previous == 0) {
					outForCpu2.write(state);
					previous = 3;
				}
				else {
					cout << "ERROR!" << endl;
				}
			}
			wait(100, SC_NS);

		}
	}

	void task4() {
		while (1) {
			state = program.read();
			if (state == 4) {
				if (previous == 0) {
					cout << "Temperatura 4" << endl;
					previous = 4;
				}
				else {
					cout << "ERROR!" << endl;
				}
			}
			wait(100, SC_NS);

		}
	}

	void task5() {
		while (1) {
			state = program.read();
			if (state == 5) {
				if (previous == 0) {
					cout << "Temperatura 5" << endl;
					previous = 5;
				}
				else {
					cout << "ERROR!" << endl;
				}
			}
			wait(100, SC_NS);

		}
	}

	void task6() {
		while (1) {
			state = program.read();
			if (state == 6) {
				if (previous == 0) {
					cout << "Temperatura 6" << endl;
					previous = 6;
				}
				else {
					cout << "ERROR!" << endl;
				}
			}
			wait(100, SC_NS);

		}
	}

	void task7() {
		while (1) {
			state = program.read();
			if (state == 7) {
				if (previous == 0) {
					cout << "Temperatura 7" << endl;
					previous = 7;
				}
				else {
					cout << "ERROR!" << endl;
				}
			}
			wait(100, SC_NS);

		}
	}


	SC_CTOR(CPU1) {
		SC_THREAD(task0);
		SC_THREAD(task1);
		SC_THREAD(task2);
		SC_THREAD(task3);
		SC_THREAD(task4);
		SC_THREAD(task5);
		SC_THREAD(task6);
		SC_THREAD(task7);
	}
};
